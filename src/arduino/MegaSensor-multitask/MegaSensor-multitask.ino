#include <Adafruit_GFX.h>
#include <Adafruit_SSD1331.h>
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define sclk 52
#define mosi 51
#define cs   10
#define rst  9
#define dc   8

#define pcbv 0004
// pcbv1 : 1 sensor DS18B20
// pcbv2 : 2 sensors DS18B20
// pcbv3 : add loop counter
// pcbv4 : add pushbutton


#define ONE_WIRE_BUS 7

int count = 0 ;
int button = 0;

int ledPin = LED_BUILTIN; // choose the pin for the LED
int inPin = 13;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status

// Definizione dei colori
#define  BLACK           0x0000
#define BLUE            0x001F
#define RED             0xF800
#define GREEN           0x07E0
#define   CYAN            0x07FF
#define   MAGENTA         0xF81F
#define   YELLOW          0xFFE0
#define   WHITE           0xFFFF

Adafruit_SSD1331 display = Adafruit_SSD1331(cs, dc, rst);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
int deviceCount = 0;
float tempC;

void setup(void) {
  pinMode(ledPin, OUTPUT);  // declare LED as output
  pinMode(inPin, INPUT);    // declare pushbutton as input
  
  Serial.begin(115200);
  sensors.begin();  // Start up the library
  display.begin();
  Serial.println("Initialisation...");

  display.fillScreen(BLACK);
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  display.print("Init.");
  
  Serial.print("Locating devices...");
  Serial.print("Found ");
  
  display.setCursor(0, 8);
  display.print("Found:");
  
  deviceCount = sensors.getDeviceCount();
  
  Serial.print(deviceCount, DEC);
  Serial.println(" DS18B20");
  Serial.println("");

  display.print(deviceCount, DEC);
  display.print(" DS18B20");

  delay(1000);
}

void loop() {

  val = digitalRead(inPin);  // read input value
  if (val == HIGH) {         // check if the input is HIGH (button released)
    button++;
    digitalWrite(ledPin, LOW);  // turn LED OFF
  } else {
    digitalWrite(ledPin, HIGH);  // turn LED ON
  } 

   sensors.requestTemperatures();
  
  display.fillScreen(BLACK);
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  display.print("Temp1:");
  //display.setCursor(5, 16);
  display.setTextColor(YELLOW);
  tempC = sensors.getTempCByIndex(0);
  display.print(tempC,1);
  display.setCursor(0, 8);
  display.setTextColor(WHITE);
  display.print("Temp2:");
  display.setTextColor(YELLOW);
  tempC = sensors.getTempCByIndex(1);
  display.print(tempC,1);

  display.setCursor(0, 16);
  display.setTextColor(WHITE);
  display.print("MQ-6:");
  display.setTextColor(YELLOW);
  display.print("N.C.");

  display.setCursor(0, 24);
  display.setTextColor(WHITE);
  display.print("Loops:");
  display.setTextColor(YELLOW);
  display.print(count++);

  display.setCursor(0, 32);
  display.setTextColor(WHITE);
  display.print("Button:");
  display.setTextColor(YELLOW);
  display.print(button);
  
  display.drawFastHLine(0, 50, display.width() - 1, RED);
  display.setCursor(0, 52);
  display.setTextColor(GREEN);
  display.setTextSize(1);
  display.print("Sensors v");
  display.print(pcbv);
  delay(1500);
}
